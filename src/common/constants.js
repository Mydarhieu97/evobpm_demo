const Constants = {
    API_URL: "https://reqres.in/api",
    PROJECT: "/project"
};

export default Constants;