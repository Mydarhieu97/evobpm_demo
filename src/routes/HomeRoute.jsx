import React from 'react';
import { Switch, Route } from 'react-router-dom'
import HomePage from '../pages/Home/HomePage';
import PATH from '../common/path';

function HomeRoute() {
    return (
        <Switch>
            <Route exact path={PATH.HOME} component={HomePage} />
        </Switch>
    );
}

export default HomeRoute;