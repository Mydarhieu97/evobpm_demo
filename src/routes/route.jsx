import React from 'react';
import { BrowserRouter } from 'react-router-dom'
import HomeRoute from './HomeRoute';


function Route() {
    return (
        <BrowserRouter>
            <HomeRoute/>
        </BrowserRouter>
    );
}

export default Route;