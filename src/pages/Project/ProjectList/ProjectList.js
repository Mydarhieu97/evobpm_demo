import React, { useState, useEffect } from 'react';
import projectService from '../../../apis/project.service';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import { Button } from '@material-ui/core';
import alertify from 'alertifyjs';
import ProjectItem from '../ProjectItem/ProjectItem';

const ProjectList = () => {
  const [currentProjectID, setCurrentProjectID] = useState('');
  const [projects, setProjects] = useState([]);

  useEffect(() => {
    const fetchListProject = async () => {
      const res = await projectService.getProjects();
      setProjects(res.data);
    }
    fetchListProject();
  }, []);

  const handleEdit = (id) => {
    setCurrentProjectID(id);
  }

  const handleDelete = (id) => {
    setCurrentProjectID(id);
    alertify.confirm('Delete Project!', 'Delete???', async () => {
      const res = await projectService.deleteProject(id);
      if (res.status == '200' && res.data == true) {
        alertify.success('Delete Successfully!!!');
        setProjects(projects.filter((p) => p.projectID !== id))
      };
    }, { setProjects });
  }

  return (
    <TableContainer component={Paper}>
      <Table aria-label="customized table">
        <TableHead>
          <TableRow>
            <TableCell>Action</TableCell>
            <TableCell>Project Key</TableCell>
            <TableCell align="right">Project Name</TableCell>
            <TableCell align="right">Start Date</TableCell>
            <TableCell align="right">End Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {projects.map((project) => (
            <ProjectItem project={project} handleDelete={handleDelete} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default ProjectList;