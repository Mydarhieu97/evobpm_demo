import { Button, TableCell, TableRow } from '@material-ui/core';
import { Edit, DeleteOutline } from '@material-ui/icons';
import React from 'react';


const ProjectItem = (props) => {
    return (
        <TableRow key={props.project.projectID}>
            <TableCell>
                <Button onClick={()=>props.handleDelete(props.project.projectID)}>
                    <Edit />
                </Button>
                <Button onClick={()=>props.handleDelete(props.project.projectID)}>
                    <DeleteOutline />
                </Button>
            </TableCell>
            <TableCell component="th" scope="row">
                {props.project.projectKey}
            </TableCell>
            <TableCell align="right">{props.project.projectName}</TableCell>
            <TableCell align="right">{props.project.startDate}</TableCell>
            <TableCell align="right">{props.project.endDate}</TableCell>
        </TableRow>
    );
}

export default ProjectItem;