import React from 'react';
import MainLayout from '../../layouts/MainLayout';
import ProjectList from '../Project/ProjectList/ProjectList';

function HomePage() {
    return (
    <div>
        <MainLayout>
            <div>
                <ProjectList />
            </div>
        </MainLayout>
    </div>
    )
}

export default HomePage;