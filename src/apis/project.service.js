import axiosClient from "./axiosClient";
import constants from "../common/constants";

const projectService = {
    getProjects: (params) => {
        const url = constants.PROJECT;
        return axiosClient.get(url, {
            params
        });
    },
    getProject: (id) => {
        const url = constants.PROJECT + `/${id}`;
        return axiosClient.get(url);
    },
    deleteProject: (id) => {
        const url = constants.PROJECT + `/${id}`;
        return axiosClient.delete(url);
    },
    editProject: (id, data) => {
        const url = constants.PROJECT + `/${id}`;
        return axiosClient.put(url, data);
    }
};
export default projectService;