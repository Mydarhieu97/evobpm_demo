import React from 'react';
import Header from '../components/Header/Header';

function MainLayout(props) {
    const { children } = props;
    return (
        <div>
            <Header/>
            <div>{children}</div>
        </div>
    );
}

export default MainLayout;