I.Setup project
    1.Download And Install NodeJS, Visual Studio Code.
        A.Download And Install NodeJS
            You can download NodeJS version appropriate for your PC at this this official link(https://nodejs.org/en/download/). You can follow this guide (https://phoenixnap.com/kb/install-node-js-npm-on-windows) for install NodeJS and NPM.
            To check: after completely install NodeJs, type this command in cmd
            Ex: node -v; npm -v
        B.Download And Install Visual Studio Code.
            You can download official version of vscode at this link(https://code.visualstudio.com/download) and follow this guide(https://code.visualstudio.com/docs/setup/windows) for install vscode.
    2.Setup React Project.
        You have two way to setup a react project:
        A.Setup normal with CMD:
            - Step1: cd to your folder where you want store your react project and open CMD by typing “CMD” in the directory path bar.
            - Step2: in command prompt(cmd) type “npx create-react-app <your_react_app_name>”
            Ex: npx create-react-app demo (demo is your react app’s name).
            - Step3: Once the installation is complete, you will also see the application returns us how to launch ReactJS.
            Now, type “cd demo” after is “npm start” to run your first react application.
            or you can check the project by command:
             - npm start => to start react project.
             - npm run build => Bundles the app into static files for production
             - npm test => Starts the test runner
             - npm run eject => removes this tool and copies build dependencies, configuration files and scripts into the app directory. If you do this you can't go back!
        B.Setup with vscode.
            - Step1: cd to your folder where you want store your react project and open CMD by typing “CMD” in the directory path bar.
            - Step2: when cmd window appears, type “code .” to open vscode at this folder.
            - Step3: in vscode, you can use shortcut key “Ctrl + `” to open terminal.
            - Step4: now you can type “npx create-react-app demo” to create new react application
            - Step5: now type “cd demo” => enter and “npm start” => enter to run your react application.
II.Visual Studio Code Extension.
    1.How To Install More Extension in Visual Studio Code.
        - Open Visual Studio Code and you will see the left side bar(active bar), click to extension icon and type name of extension which you want install to search box to find and install it.
    2.Some Visual Studio Code Extension.
        - Auto Close Tag
        - Atom One Dark Theme
        - Bookmarks
        - Bracket Pair Colorizer
        - Code Spelling Checker
        - EditorConfig
        - ES7 React/Redux/React-Native/JS snippets
        - ESLint
        - GitLens
        - html to JSX
        - Live Server
        - Prettier
        - Material Icon Theme
        - vscode-styled-components
        - SVG Viewer- TsLinst (for coed for)
III.React Tool
    1.How To Add More Tool In Your React Application.
        - Way 1: cd to your react app folder, and open CMD here, after type command to install what tool you want add to your project by command “npm install <your_tool_pack_name>”. Example i want install Material Core to have a nice UI, i will type “npm install @material-ui/core”.
        - Way 2: open vscode at your project foler, open terminal and cd to your project folder and type like in terminal like the way 1: “npm install <your_tool_pack_name>” and wait some minute.
    2.Some React Tool
        - Material (core, icons, lab, styles) => for more nice UI for your react application (you can read docs at https://material-ui.com/getting-started/installation/)
        - AlertifyJS (option) => for nice alert, confirm, ... dialog. (you can read docs at https://alertifyjs.com/guide.html)
        - Axios (option)
